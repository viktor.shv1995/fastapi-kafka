## Run and test

To start the application just run (Docker compose V1)
```
docker-compose up -d
```
Docker compose V2
```
docker compose up -d
```

To post messages for the producer access swagger 
```
http://localhost:8080/kafka_producer/docs
```

To see the messages consumed by the consumer (Docker compose V1)
```
docker-compose logs consumer
```
Docker compose V2
```
docker compose logs consumer
```
